from typing import Optional

from rich import print

from . import gitlab_api
from .arguments import parse_arguments
from .config import Config


def main():
    args = parse_arguments()
    config = Config.load()

    if args.setup:
        _run_setup(config)
        exit(0)

    elif not config.is_valid():
        print("No valid configuration found. Need to run setup.")
        _run_setup(config)

    if not config.is_valid():
        raise RuntimeError("Invalid configuration. Can not proceed.")

    gl = gitlab_api.connect(config)

    projects = gitlab_api.fetch_projects(args, gl)
    if not projects:
        print("No projects found. Aborting.")
        exit(1)

    if args.show_projects_only:
        names = [p.path_with_namespace for p in projects]
        print(f"The following {len(names)} projects would searched:")
        for name in sorted(names):
            print(name)
        exit(0)

    for num, search_term in enumerate(args.search_term, start=1):
        print()
        matches = gitlab_api.search_projects(
            args, gl, projects, search_term, num, len(args.search_term)
        )
        _show_search_results(args, matches, config)


def _run_setup(config: Config):
    """Request setup information from user and store them."""
    print("Setting up gitlab-search")
    try:
        while True:
            config.token = _input_with_default(
                "Please enter the GitLab token", config.token
            )
            if config.token:
                break
        config.url = _input_with_default("Please enter the GitLab URL", config.url)
    except KeyboardInterrupt:
        print()
        exit(1)
    path = config.save()
    print(f"Setup complete. Config stored at {path}")


def _input_with_default(question, default: Optional[str] = None) -> Optional[str]:
    response = input(f"{question} [{default if default else ''}]: ")
    return response or default


def _show_search_results(args, matches, config: Config):
    """Show search results."""
    if not len(matches):
        print("Found no matches.")
        return

    print(f"Found {len(matches)} matches:")
    for detail in sorted(matches, key=lambda o: o.sort_key):
        print(detail.gitlab_link(config.url))
        if args.show_details:
            print(detail.data)
            print("---")


if __name__ == "__main__":
    main()
