from dataclasses import dataclass
from pathlib import Path
from typing import List, Set

import gitlab
from gitlab.const import SearchScope
from gitlab.exceptions import GitlabSearchError
from rich import print
from rich.progress import track

from .config import Config


@dataclass
class SearchMatch:
    """A match of the search term in a file."""

    project_path: str
    file_path: str
    start_line: str
    ref: str
    data: str

    @property
    def sort_key(self) -> str:
        return f"{self.project_path}-{self.file_path}-{self.start_line:09d}"

    def gitlab_link(self, gitlab_url: str) -> str:
        return (
            f"{gitlab_url}/{self.project_path}/-/blob/"
            f"{self.ref}/{self.file_path}#L{self.start_line}"
        )

    @classmethod
    def from_search_item(cls, project, item: dict) -> "SearchMatch":
        return cls(
            project_path=project.path_with_namespace,
            file_path=item["path"],
            start_line=item["startline"],
            ref=item["ref"],
            data=item["data"],
        )


def connect(config: Config) -> gitlab.Gitlab:
    print(f"Connecting to {config.url}...")
    gl = gitlab.Gitlab(config.url, private_token=config.token)
    gl.auth()
    username = gl.user.username if gl.user else "?"
    print(f"Connected as {username}")
    return gl


def fetch_projects(args, gl: gitlab.Gitlab):
    """Fetch projects to search in."""
    projects = []
    exclude_projects = set(args.exclude_projects) if args.exclude_projects else set()
    projects += _fetch_group_projects(args, gl, exclude_projects)
    projects += _fetch_user_projects(args, gl, exclude_projects)
    return projects


def _fetch_group_projects(args, gl: gitlab.Gitlab, exclude_projects: Set[str]):
    projects = []
    if not args.exclude_groups:
        username = gl.user.username if gl.user else "?"
        print(f"Fetching group projects from {username}...")
        for group in gl.groups.list(order_by="name", sort="asc", iterator=True):
            for project in group.projects.list(
                order_by="name", sort="asc", iterator=True
            ):
                if (
                    not exclude_projects
                    or project.path_with_namespace not in exclude_projects
                ):
                    projects.append(project)
    return projects


def _fetch_user_projects(args, gl: gitlab.Gitlab, exclude_projects: Set[str]):
    projects = []
    users = _determine_users(args, gl)
    for user in users:
        print(f"Fetching personal projects from {user.username}...")
        for project in user.projects.list(
            owned=True, order_by="name", sort="asc", iterator=True
        ):
            if (
                not exclude_projects
                or project.path_with_namespace not in exclude_projects
            ):
                projects.append(project)
    return projects


def _determine_users(args, gl: gitlab.Gitlab):
    users = []
    if not args.exclude_personal_projects and gl.user:
        user = gl.users.get(id=gl.user.id)
        users.append(user)
    if args.include_users:
        for user_id in args.include_users:
            user = gl.users.get(id=user_id)
            users.append(user)
    return users


def search_projects(
    args, gl: gitlab.Gitlab, projects, search_term: str, num: int, total: int
) -> List[SearchMatch]:
    """Search files in projects."""
    if total > 1:
        search_text = f"{search_term} ({num}/{total})"
    else:
        search_text = search_term
    matches = []
    print(f"Searching {len(projects)} projects for: {search_text}")
    require_extensions = (
        set(args.require_extensions) if args.require_extensions else set()
    )
    for project in track(projects, description=search_text):
        project = gl.projects.get(id=project.id)
        for _ in range(2):
            new_matches = []
            try:
                for item in project.search(
                    SearchScope.BLOBS, search_term, iterator=True
                ):
                    item_suffix = Path(item["path"]).suffix[1:]
                    if not require_extensions or item_suffix in require_extensions:
                        new_matches.append(SearchMatch.from_search_item(project, item))
            except GitlabSearchError:
                pass
            else:
                matches += new_matches
                break
        else:
            print(f"ERROR: Failed to search project {project.path_with_namespace}")
    return matches
